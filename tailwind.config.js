/** @type {import('tailwindcss').Config} */
/** @type {import('tailwindcss').Config} */
const defaultTheme = require('tailwindcss/defaultTheme');
module.exports = {
  content: ['./src/**/*.{html,js}'],
  theme: {
    screens: {
      'xs': '375px',
      ...defaultTheme.screens
    },
    extend: {
      fontFamily: {
        'Manrope': ['manrope', 'sans-serif']
      },
      fontSize: {
        'custom-xs': '.80rem',
      },
      colors: {
        'dark-greyish-blue': 'hsl(217, 19%, 35%)',
        'desaturated-dark-blue': 'hsl(214, 17%, 51%)',
        'light-greyish-blue': 'hsl(210, 46%, 95%)',
        'greyish-blue': 'hsl(212, 23%, 69%)'
      }
    },
  },
  plugins: [],
}
