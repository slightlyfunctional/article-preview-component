import { createPopper } from '@popperjs/core'; // /lib/popper-lite -> does not include offset fn

document.addEventListener('DOMContentLoaded', handleResize);
window.addEventListener('resize', handleResize);

function handleResize (e) {
  const popcorn = document.querySelector('#share-popper');
  if (popcorn.classList.contains('active')) {
    popcorn.click(); // emulate click to deactivate popper state
    popcorn.classList.remove('active');
  }
  // Add listener depending on viewport
  if (window.innerWidth >= 1024) { // lg -> xxl
    popcorn.removeEventListener('click', activateShareRow);
    popcorn.addEventListener('click', activatePopper);
  } else { // xs -> lg
    popcorn.removeEventListener('click', activatePopper);
    popcorn.addEventListener('click', activateShareRow);
  }
}

// mobile popper
function activateShareRow (e) {
  e.preventDefault();
  // Add active class to maintain state
  document.querySelector('#share-popper').classList.toggle('active');
  // Hide profile
  const container = document.querySelector('#profile');
  container.classList.toggle('hidden');
  container.parentElement.classList.toggle('bg-dark-greyish-blue')
  document.querySelector('#share-tooltip').classList.toggle('hidden');
  const popper = document.querySelector('#share-popper');
  // Color svg
  popper.querySelector('svg > path').classList.toggle('fill-white');
  popper.classList.toggle('bg-light-greyish-blue');
  popper.classList.toggle('bg-desaturated-dark-blue');
}

// desktop popper
function activatePopper (e) {
  e.preventDefault();
  // Change button bg
  const popcorn = document.querySelector('#share-popper');
  popcorn.classList.toggle('bg-light-greyish-blue');
  popcorn.classList.toggle('bg-desaturated-dark-blue');
  // Add active class to maintain state
  popcorn.classList.toggle('active');
  // Color svg
  popcorn.querySelector('svg > path').classList.toggle('fill-white');
  const tooltip = document.querySelector('#share-tooltip-desktop');
  createPopper(popcorn, tooltip, {
    modifiers: [
      {
        name: 'offset',
        enabled: true,
        phase: 'main',
        options: {
          offset: [0, 20],
        },
      },
    ],
    placement: 'top',
  });
  tooltip.classList.toggle('hidden');
}
